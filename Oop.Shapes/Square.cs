﻿using System;

namespace Oop.Shapes
{
    public class Square : Shape
	{
		private double side;
		public double Side
        {
			get => side;
			set
            {
				if (value > 0)
                {
					side = value;
				}
				else
                {
					throw new ArgumentOutOfRangeException(nameof(side), "Вводные данные не могут быть отрицательными или равными нулю");
                }
            }
        }
		public Square(double a)
        {
			if (a < 1)
            {
				throw new ArgumentOutOfRangeException(nameof(a), "Вводные данные не могут быть отрицательными или равными нулю");
            }
			Side = a;
			Area = a * a;
			Perimeter = a * 4;
			VertexCount = 4;
        }

		public override bool IsEqual(Shape shape)
		{
			if (this.Area == shape.Area)
			{
				return true;
			}
			return false;
		}
	}
}
