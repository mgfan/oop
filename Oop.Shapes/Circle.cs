﻿using System;

namespace Oop.Shapes
{
    public class Circle : Shape
	{
		private double radius;
		public double Radius
        {
            get => radius;
            set
            {
                if (value > 0)
                {
                    radius = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(radius), "Вводные данные не могут быть отрицательными или равными нулю");
                }
            }
        }

        public Circle(double radius)
		{
            Radius = radius;
			Area = Math.PI * radius * radius;
			Perimeter = 2 * Math.PI * radius;
			VertexCount = 0;
		}
		public override bool IsEqual(Shape shape)
		{
			if (this.Area == shape.Area)
			{
				return true;
			}
			return false;
		}
	}
}
