﻿using System;

namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(double radius)
		{
			Shape circle = new Circle(radius);
			return circle;
		}
		public Shape CreateTriangle(double a, double b, double c)
		{
			Shape triangle = new Triangle(a, b, c);
			return triangle;
		}
		public Shape CreateSquare(double a)
		{
			Shape square = new Square(a);
			return square;
		}
		public Shape CreateRectangle(double a, double b)
		{
			Shape rectangle = new Rectangle(a, b);
			return rectangle;
		}
	}
}