﻿using System;

namespace Oop.Shapes
{
    public class Rectangle : Shape
	{
		private double sideA;
		public double SideA
        {
			get => sideA;
			set
            {
				if (value > 0)
                {
					sideA = value;
                }
				else
                {
					throw new ArgumentOutOfRangeException(nameof(sideA), "Вводные данные не могут быть отрицательными или равными нулю");
                }
            }
        }

		private double sideB;
		public double SideB
		{
			get => sideB;
			set
			{
				if (value > 0)
				{
					sideB = value;
				}
				else
				{
					throw new ArgumentOutOfRangeException(nameof(sideB), "Вводные данные не могут быть отрицательными или равными нулю");
				}
			}
		}
		public Rectangle(double a, double b)
        {
			SideA = a;
			SideB = b;
			Area = a * b;
			Perimeter = (a + b) * 2;
			VertexCount = 4;
        }

		public override bool IsEqual(Shape shape)
		{
			if (this.Area == shape.Area)
			{
				return true;
			}
			return false;
		}
	}
}
