﻿using System;

namespace Oop.Shapes
{
    public class Triangle : Shape
	{
		private double sideA;
		public double SideA
        {
			get => sideA;
			set
            {
				if (value > 0)
                {
					sideA = value;
                }
				else
                {
					throw new System.ArgumentOutOfRangeException(nameof(sideA), "Вводные данные не могут быть отрицательными или равными нулю");
                }
            }
        }
		private double sideB;
		public double SideB
		{
			get => sideB;
			set
			{
				if (value > 0)
				{
					sideB = value;
				}
				else
				{
					throw new System.ArgumentOutOfRangeException(nameof(sideB), "Вводные данные не могут быть отрицательными или равными нулю");
				}
			}
		}
		private double sideC;
		public double SideC
		{
			get => sideC;
			set
			{
				if (value > 0)
				{
					sideC = value;
				}
				else
				{
					throw new System.ArgumentOutOfRangeException(nameof(sideC), "Вводные данные не могут быть отрицательными или равными нулю");
				}
			}
		}
		public Triangle(double a, double b, double c)
        {
			SideA = a;
			SideB = b;
			SideC = c;
			if (a + b < c || a + c < b || b + c < a)
			{
				throw new System.InvalidOperationException("Некорректные стороны треугольника");
			}
			double poluperimetr = (a + b + c) / 2;
			Area = Math.Sqrt(poluperimetr * (poluperimetr - a) * (poluperimetr - b) * (poluperimetr - c));
			Perimeter = a + b + c;
			VertexCount = 3;
		}
		public override bool IsEqual(Shape shape)
		{
			if (this.Area == shape.Area)
			{
				return true;
			}
			return false;
		}
	}
}
