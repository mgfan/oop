﻿namespace Oop.Shapes
{
    public abstract class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public double Area { get; protected set; }

		/// <summary>
		/// Периметр
		/// </summary>
		public double Perimeter { get; protected set; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		public int VertexCount { get; protected set; }

		public virtual bool IsEqual(Shape shape) => false;
	}
}
